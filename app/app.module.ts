import { NgModule }      from '@angular/core';
import { BrowserModule } from '@angular/platform-browser';
import {RouterModule} from "@angular/router";
import {HttpModule} from "@angular/http";
import {FormsModule} from "@angular/forms";
import {AppComponent} from "./app.component";

// NgModule decorator
@NgModule({
    imports:        [ BrowserModule, RouterModule, HttpModule, FormsModule ],
    declarations:  [ AppComponent ],
    bootstrap:      [ AppComponent ]
})
export class AppModule { }  // exported to be loaded by main.ts
